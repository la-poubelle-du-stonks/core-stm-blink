#include "core/main.h"

#include "core/core.hpp"

uint8_t canErrFlag = 0;
uint32_t canErrCode = 0;
uint8_t resetFlag = 0;
uint8_t rxCount = 0;
uint8_t forceReconnect = 0;
CAN_HandleTypeDef hcan1;

Hermes_t hermes;
Talos_t talos;
Core::SelfCheck *coreSelfCheck;
Core::RepeatingTimer *repeatingTimer;

void setup() {
    HAL_Init();

    Serial.begin(115200);

    Talos_commands initialCommand = doNothing;

    Hermes_init(&hermes, DEVICE_ID, 8);

    if (HermesCAN_setup(&hermes, &hcan1) == HERMES_CAN_ERROR) {
        Core_log(MOTHER_ID, "CAN setup failed.");
        initialCommand = error;
    }

    if (HermesCAN_setupIt(&hcan1) == HERMES_CAN_ERROR) {
        Core_log(MOTHER_ID, "CAN IT setup failed.");
        initialCommand = error;
    }

    if (DEVICE_ID == 0) {
        Core_log(MOTHER_ID,
                 "WARNING: You are using the default DEVICE_ID, please change "
                 "it in the platformio.ini file.");
    }

    coreSelfCheck = new Core::SelfCheck(&hermes);
    coreSelfCheck->enable();

    repeatingTimer = new Core::RepeatingTimer();

    Talos_init(&talos);
    talos.command = initialCommand;

    setjmp(coreSelfCheck->jmpLoc);
    if (coreSelfCheck->shallEscape()) {
        talos.command = error;
        Serial.println("freezed in user init");
    }

    Talos_exec(&talos);  // Execute init user code here.

    coreSelfCheck->loopCheck();
    coreSelfCheck->callback();
}

void loop() {
    setjmp(coreSelfCheck->jmpLoc);
    if (coreSelfCheck->shallEscape()) {
        talos.command = error;
        Serial.println("freezed in user loop");
    }

    canErrCode = HAL_CAN_ERROR_NONE;

    if (canErrFlag) {
        canErrCode = HAL_CAN_GetError(&hcan1);
        HAL_CAN_ResetError(&hcan1);
        canErrFlag = 0;
        Core_log(MOTHER_ID,
                 "Reported CAN error: " + std::to_string(canErrCode));
    }

    if ((Hermes_waitingForTX(&hermes) &&
         (HermesCAN_processTX(&hermes, &hcan1) == HERMES_CAN_OK)) ||
        forceReconnect == 1) {
        if (talos.currentState == DISCONNECTED) {
            talos.command = CANConnected;
            forceReconnect = 0;
        }
    }

    // process errors here
    if (canErrCode > 0) {
        talos.command = error;
    }

    // process CAN RX
    rxCount = Hermes_getCompleteBufferCount(&hermes);
    while (rxCount != 0) {
        Hermes_triggerNextMessage(&hermes);
        rxCount--;
    }

    Core_priorityLoop();

    if (repeatingTimer->is_time_up()) {
        Talos_exec(&talos);  // Execute init user code here.
    }

    coreSelfCheck->loopCheck();
}

void HAL_CAN_MspInit(CAN_HandleTypeDef *hcan) {
    /* CLOCK */
    __HAL_RCC_CAN1_CLK_ENABLE();

    /* GPIO */
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_11, LL_GPIO_MODE_ALTERNATE);  // RX
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_11, LL_GPIO_PULL_UP);
    LL_GPIO_SetAFPin_8_15(GPIOA, LL_GPIO_PIN_11, LL_GPIO_AF_9);  // AF9 CAN D10

    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_12, LL_GPIO_MODE_ALTERNATE);  // TX
    LL_GPIO_SetAFPin_8_15(GPIOA, LL_GPIO_PIN_12, LL_GPIO_AF_9);  // AF9 CAN D2

    /* Interrupts */
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);

    HAL_NVIC_SetPriority(CAN1_RX1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);

    HAL_NVIC_SetPriority(CAN1_TX_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_TX_IRQn);

    HAL_NVIC_SetPriority(CAN1_SCE_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_SCE_IRQn);
}

void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processTX(&hermes, hcan) == HERMES_CAN_OK) {
        if (talos.currentState == DISCONNECTED) {
            talos.command = CANConnected;
        }
    }
}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processTX(&hermes, hcan) == HERMES_CAN_OK) {
        if (talos.currentState == DISCONNECTED) {
            talos.command = CANConnected;
        }
    }
}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processTX(&hermes, hcan) == HERMES_CAN_OK) {
        if (talos.currentState == DISCONNECTED) {
            talos.command = CANConnected;
        }
    }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan) { canErrFlag = 1; }

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processRX(&hermes, hcan, CAN_RX_FIFO0) == HERMES_CAN_OK) {
        if (talos.currentState == DISCONNECTED) {
            talos.command = CANConnected;
        }
    }
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processRX(&hermes, hcan, CAN_RX_FIFO1) == HERMES_CAN_OK) {
        if (talos.currentState == DISCONNECTED) {
            talos.command = CANConnected;
        }
    }
}

void Hermes_onReservedMessage(Hermes_t *hermes, HermesBuffer *buffer) {
    switch (buffer->command) {
        case CORE_CMD_TALOS_RESET:

            if (buffer->remote != MOTHER_ID) {
                // Only MOTHER_ID can reset a peripheral
                break;
            }
            Core_log(MOTHER_ID, "Remote soft reset requested by " + MOTHER_ID);
            Core_softReset();
            break;

        case CORE_CMD_TALOS_STATUS:

            Hermes_send(hermes, buffer->remote, buffer->command, 1, "o",
                        talos.currentState);
            break;

        default:
            break;
    }
}

/**Core generic functions */

void Core_log(uint8_t receiver, std::string message) {
    Hermes_log(&hermes, receiver, (char *)message.c_str());
#ifdef CORE_LOG_SERIAL
    Serial.println((char *)message.c_str());
#endif
}

void Core_hardReset() {
    delay(1000);
    HAL_NVIC_SystemReset();
}

void Core_softReset() {
    coreSelfCheck->reset();
    Hermes_init(&hermes, DEVICE_ID, 8);
    talos.command = reset;
    forceReconnect = 1;
}

void Core_forceError() { talos.command = error; }

void Core_setMaximumLoopFrequency(unsigned long frequency) {
    repeatingTimer->disable();
    repeatingTimer->setFrequency(frequency);
    repeatingTimer->enable();
}

void Core_setMinimumLoopPeriod(unsigned long us){
    repeatingTimer->disable();
    repeatingTimer->setPeriod(us);
    repeatingTimer->enable();
}