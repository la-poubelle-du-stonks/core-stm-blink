#include "core/main.h"

extern CAN_HandleTypeDef hcan1;

/**
 * HAL RX0 Interrupt Handler.
 * Steer interrupts to the corresponding handler.
 */
void CAN1_RX0_IRQHandler(void) { HAL_CAN_IRQHandler(&hcan1); }

/**
 * HAL CAN RX1 Interrupt Handler.
 * Steer interrupts to the corresponding handler.
 */
void CAN1_RX1_IRQHandler(void) { HAL_CAN_IRQHandler(&hcan1); }

/**
 * HAL CAN TX Interrupt Handler.
 * Steer interrupts to the corresponding handler.
 */
void CAN1_TX_IRQHandler(void) { HAL_CAN_IRQHandler(&hcan1); }

/**
 * HAL CAN Errors Interrupt Handler.
 * Steer interrupts to the corresponding handler.
 */
void CAN1_SCE_IRQHandler(void) { HAL_CAN_IRQHandler(&hcan1); }