#include "core/HermesCAN.h"

HermesCANReturnCode HermesCAN_setup(Hermes_t *hermes,
                                    CAN_HandleTypeDef *hcan1) {
    CAN_FilterTypeDef filter;
    CAN_FilterTypeDef broadcastFilter;

    if (!IS_CAN_MODE(HERMES_CAN_MODE)) {
        return HERMES_CAN_ERROR;
    }

    hcan1->Instance = CAN1;
    hcan1->Init.Mode =
        HERMES_CAN_MODE;  // FORCE RX TO HIGH VERY IMPORTANT IF TIMEOUT ERROR
                          // (0x60000) IN LOOPBACK MODE

    // 80MHz
    hcan1->Init.Prescaler = 16;               // 32 MHZ : 8
    hcan1->Init.SyncJumpWidth = CAN_SJW_1TQ;  // CAN_SJW_1TQ
    hcan1->Init.TimeSeg1 = CAN_BS1_2TQ;       // CAN_BS1_2TQ
    hcan1->Init.TimeSeg2 = CAN_BS2_2TQ;       // CAN_BS2_1TQ //VOIR hcan1 JULIEN
    hcan1->Init.AutoRetransmission = ENABLE;
    hcan1->Init.TimeTriggeredMode = DISABLE;
    hcan1->Init.AutoWakeUp = DISABLE;
    hcan1->Init.AutoBusOff = ENABLE;
    hcan1->Init.TransmitFifoPriority = ENABLE;
    hcan1->Init.ReceiveFifoLocked = DISABLE;

    if (HAL_CAN_Init(hcan1) != HAL_OK) {
        return HERMES_CAN_ERROR;
    }

    /* Device filter setup */
    filter.FilterActivation = CAN_FILTER_ENABLE;
    filter.FilterBank = 0;
    filter.FilterFIFOAssignment = CAN_RX_FIFO0;
    filter.FilterMode = CAN_FILTERMODE_IDMASK;
    filter.FilterScale = CAN_FILTERSCALE_32BIT;

    filter.FilterMaskIdHigh =
        0xFF << 5;  // 0x 0001 1111 1110 0000   bits at 1 must match the
                    // corresponding defined in filterId.
    filter.FilterMaskIdLow = 0x0;

    filter.FilterIdHigh =
        hermes->id << 5;  // 0x 0001 0000 0010 0000   if deviceId is set to 1.
    filter.FilterIdLow = 0x0;

    if (HAL_CAN_ConfigFilter(hcan1, &filter) != HAL_OK) {
        return HERMES_CAN_ERROR;
    }

    /* Broadcast device setup */
    broadcastFilter.FilterActivation = CAN_FILTER_ENABLE;
    broadcastFilter.FilterBank = 1;
    broadcastFilter.FilterFIFOAssignment = CAN_RX_FIFO1;
    broadcastFilter.FilterMode = CAN_FILTERMODE_IDMASK;
    broadcastFilter.FilterScale = CAN_FILTERSCALE_32BIT;

    filter.FilterMaskIdHigh =
        0xFF << 5;  // 0x 0001 1111 1110 0000   bits at 1 must match the
                    // corresponding defined in filterId.
    filter.FilterMaskIdLow = 0x0;

    filter.FilterIdHigh =
        BROADCAST_ID << 5;  // 0x 0001 1111 1110 0000   if deviceId is set to 1.
    filter.FilterIdLow = 0x0;

    if (HAL_CAN_ConfigFilter(hcan1, &broadcastFilter) != HAL_OK) {
        return HERMES_CAN_ERROR;
    }

    if (HAL_CAN_Start(hcan1) != HAL_OK) {
        return HERMES_CAN_ERROR;
    }

    return HERMES_CAN_OK;
}

HermesCANReturnCode HermesCAN_setupIt(CAN_HandleTypeDef *hcan1) {
    if (HAL_CAN_ActivateNotification(hcan1, CAN_ITS) != HAL_OK) {
        return HERMES_CAN_ERROR;
    }
    return HERMES_CAN_OK;
}

HermesCANReturnCode HermesCAN_processTX(Hermes_t *hermes,
                                        CAN_HandleTypeDef *hcan1) {
    uint8_t remote;
    uint16_t command;
    uint32_t isResponse;
    size_t payloadSize;
    uint8_t payload[8];
    HermesReturnCode code;

    code = Hermes_sendData(hermes, &remote, &command, &isResponse, &payloadSize,
                           payload);
    if (!code) {
        CAN_TxHeaderTypeDef header;
        header.ExtId = Hermes_serializeCANFrameID(
            remote, command, hermes->id,
            isResponse);  // renommer reciever par remote
        header.IDE = CAN_ID_EXT;
        header.RTR = CAN_RTR_DATA;
        header.DLC = payloadSize;

        uint32_t txbox;
        if (HAL_CAN_AddTxMessage(hcan1, &header, payload, &txbox) != HAL_OK) {
            return HERMES_CAN_ERROR;
        }
    }

    return HERMES_CAN_OK;
}

HermesCANReturnCode HermesCAN_processRX(Hermes_t *hermes,
                                        CAN_HandleTypeDef *hcan1,
                                        uint32_t fifo) {
    CAN_RxHeaderTypeDef header;
    uint8_t payload[8];
    uint8_t sender;
    uint8_t receiver;
    uint16_t command;
    bool isResponse;
    HermesReturnCode code;

    if (HAL_CAN_GetRxMessage(hcan1, fifo, &header, payload) != HAL_OK) {
        return HERMES_CAN_ERROR;
    }

    Hermes_parseCANFrameID(header.ExtId, &receiver, &command, &sender,
                           &isResponse);

    if (receiver != hermes->id) {
        return HERMES_CAN_OK;
    }

    if (Hermes_recieveData(hermes, sender, command, payload, header.DLC,
                           isResponse) != HERMES_OK) {
        return HERMES_CAN_ERROR;
    }

    return HERMES_CAN_OK;
}