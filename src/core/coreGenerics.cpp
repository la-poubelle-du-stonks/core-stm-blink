#include "core/coreGenerics.hpp"

using namespace Core;

/** SELFCHECK **/

SelfCheck::SelfCheck(Hermes_t *hermes) {
    hermesInstance = hermes;

    tim = new HardwareTimer(TIM15);
    tim->setMode(1, TIMER_OUTPUT_COMPARE_ACTIVE, NC);
    tim->setPrescaleFactor(8000);
    tim->setOverflow(10 * CORE_SELFCHECK_WAIT_TIME_MS);
    tim->attachInterrupt([this]() { this->callback(); });
    tim->resume();

    this->reset();
}

void SelfCheck::reset() {
    canCheckpoint = 0;
    loopCheckpoint = 1;
    escape = 0;
}

SelfCheck::~SelfCheck() { this->disable(); }

void SelfCheck::enable() { enabled = true; }

void SelfCheck::disable() { enabled = false; }

void SelfCheck::canCheck() { canCheckpoint = 1; }

void SelfCheck::loopCheck() { loopCheckpoint = 1; }

bool SelfCheck::shallEscape() { return escape == 1; }

void SelfCheck::callback() {
    if(!enabled){
        return;
    }

    if (!canCheckpoint) {
        Hermes_heartbeat(hermesInstance, DEVICE_ID, false);
    }
    canCheckpoint = 0;

    if (!loopCheckpoint && !escape) {
        escape = 1;
        longjmp(jmpLoc, -1);
    }
    loopCheckpoint = 0;
}

/** REPEATING TIMER **/

RepeatingTimer::RepeatingTimer(const micro_second period)
    : __period(period),
      previousTime(micros() - period),
      enabled(true) {}

RepeatingTimer::RepeatingTimer()
    : __period(0), previousTime(micros()), enabled(false) {}

bool RepeatingTimer::enable() {
    if (enabled || __period == 0) return false;
    enabled = true;
    return true;
}

bool RepeatingTimer::disable() {
    if (!enabled || __period == 0) return false;
    enabled = false;
    return true;
}

void RepeatingTimer::setPeriod(const micro_second period) {
    __period = period;
}

void RepeatingTimer::setFrequency(const hertz frequency) {
    __period = (micro_second) (pow(10, 6) / frequency);
}

bool RepeatingTimer::is_time_up() {
    if (!enabled) return true;
    const micro_second currentTime = micros();
    if (currentTime >= previousTime + __period) {
        previousTime = currentTime;
        return true;
    } else {
        return false;
    }
}
