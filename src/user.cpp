#include "core/core.hpp"

constexpr uint16_t  cmd_set_red    = 1,
                    cmd_set_green  = 2,
                    cmd_set_blue   = 3,
                    cmd_get_analog = 4;

class Self {
     private:
     int pin_r = D6;
     int pin_g = D9;
     int pin_b = D11;
     int pin_analog = A0;
     uint8_t value;

     
     public:
     Self () {
          pinMode(pin_r, OUTPUT);
          pinMode(pin_g, OUTPUT);
          pinMode(pin_b, OUTPUT);
          pinMode(pin_analog, INPUT_ANALOG);
     }

     void set (uint8_t value) {
          this->value = value;
          digitalWrite(pin_r, (value & 1) ? HIGH : LOW);
          digitalWrite(pin_g, (value >> 1 & 1) ? HIGH : LOW);
          digitalWrite(pin_b, (value >> 2 & 1) ? HIGH : LOW);
     }

     uint8_t get () {
          return value;
     }

     uint16_t analog () {
          return analogRead(pin_analog);
     }


     void receive_buffer (HermesBuffer *buf) {
          if (buf->isResponse) {
               receive_response(buf);
               return;
          }
          receive_request(buf);
     }

     void receive_response (HermesBuffer *buf) {
          // No.
     }

     void receive_request (HermesBuffer *buf) {
          uint8_t val;
          
          switch (buf->command)
          {
          case cmd_set_red:
               Hermes_getCommandArgument(buf, &val);
               set((get() & ~1) | (val & 1));
               break;
          case cmd_set_green:
               Hermes_getCommandArgument(buf, &val);
               set((get() & ~2) | ((val & 1) << 1));
               break;
          case cmd_set_blue:
               Hermes_getCommandArgument(buf, &val);
               set((get() & ~4) | ((val & 1) << 2));
               break;

          case cmd_get_analog:
               send_analog(buf->remote);
               break;
          
          default:
               break;
          }
     }

     void send_analog (uint8_t remote) {
          Hermes_send(
               &hermes, remote, cmd_get_analog, true, "i", analog()
          );
     }

};

Self me;

void Talos_initialisation() {
    /**
     * Code to be run on startup and when exiting error state.
     * Make sure you don't init your pins twice.
     */
    me.set(7);
    delay(2000);
    me.set(0);
}

void Talos_loop() {
    /**
     * Looping code. Stopped when in error state.
     */
}

void Talos_onError() {
    /**
     * Looping code when in error state.
     */
    me.set(1);
    delay(250);
    me.set(0);
    delay(250);
}

void Core_priorityLoop() {
    /**
     * Looping code no matter what the current state is.
     */
}

void Hermes_onMessage(Hermes_t *hermesInstance, uint8_t sender,
                      uint16_t command, uint32_t isResponse) {
    /**
     * Code to be run every time a message is received over the CAN network.
     */
    HermesBuffer *buf;
    Hermes_getInputBuffer(hermesInstance, sender, command, &buf);
    me.receive_buffer(buf);
}