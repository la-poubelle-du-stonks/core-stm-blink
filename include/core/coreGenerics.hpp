#ifndef __POLYBOT_CORE_GENERICS__
#define __POLYBOT_CORE_GENERICS__

#include <Arduino.h>
#include <HardwareTimer.h>
#include <csetjmp>

#include "unit.hpp"

extern "C" {
#include "HermesCAN.h"
}

#ifndef CORE_SELFCHECK_WAIT_TIME_MS
/**
 * Maximum time before the loop is killed (in ms).
 */
#define CORE_SELFCHECK_WAIT_TIME_MS 1000
#endif

namespace Core {
class SelfCheck {
   private:
    uint8_t canCheckpoint;
    uint8_t loopCheckpoint;
    uint8_t escape;
    HardwareTimer *tim;
    Hermes_t *hermesInstance;
    bool enabled;

   public:
    /**
     * Escape point.
     */
    jmp_buf jmpLoc;

    /**
     * Enable period selfchecks.
     */
    void enable();

    /**
     * Disable period selfchecks.
     */
    void disable();

    /**
     * Proof that CAN network is up and working.
     */
    void canCheck();

    /**
     * Proof that the process is not blocked.
     */
    void loopCheck();

    /**
     * Check whether or not the CAN network is down and if a loop is blocking
     * the process.
     */
    void callback();

    /**
     * Check if we need to escape the process.
     */
    bool shallEscape();

    /**
     * Reset all parameters.
     */
    void reset();

    /**
     * Init the selfcheck class.
     * @param hermes Main Hermes instance.
     */
    SelfCheck(Hermes_t *hermes);

    /**
     * De-init the selfcheck class.
     */
    ~SelfCheck();
};

class RepeatingTimer {
   private:
    micro_second __period;
    micro_second previousTime;
    bool enabled;

   public:
    RepeatingTimer(const micro_second period);
    RepeatingTimer();
    void setPeriod(const micro_second period);
    void setFrequency(const hertz frequency);
    bool enable();
    bool disable();
    bool is_time_up();
};
}  // namespace Core
#endif