#ifndef __POLYBOT_CORE__
#define __POLYBOT_CORE__

#include <Arduino.h>

#include <string>

#include "coreGenerics.hpp"
#include "unit.hpp"

extern "C" {
#include <Talos_functionsUser.h>
#include <Talos_periphMain.h>

#include "HermesCAN.h"
}

/**
 * Reserved Core commands to be send over Hermes.
 */
typedef enum {
    CORE_CMD_TALOS_STATUS   = 4081, /**Get Talos status*/
    CORE_CMD_TALOS_RESET    = 4080  /**Force Talos to reset*/
} CoreReservedCommands;

/**
 * Main hermes instance.
 */
extern Hermes_t hermes;

/**
 * Main Talos instance.
 */
extern Talos_t talos;

/**
 * Core Selfcheck class. It is used to prevent blocking loop and failed CAN
 * link.
 */
extern Core::SelfCheck *coreSelfCheck;

/**
 * RepeatingTimer class. Used to set the maximum user loop frequency.
*/
extern Core::RepeatingTimer *repeatingTimer;

/**
 * Logs a message to Core.
 * It prints it to the Serial port if CORE_SERIAL_DEBUG is defined.
 * The message is also send over the CAN to the specified receiver (ie:
 * MOTHER_ID).
 * @param receiver ID of the receiver device.
 * @param message Message to send.
 */
void Core_log(uint8_t receiver, std::string message);

/**
 * Reset the whole STM. It performs the same as you pressed the reset button.
 */
void Core_hardReset();

/**
 * Set Talos to reset mode.
 */
void Core_softReset();

/**
 * Set Talos to error mode.
 */
void Core_forceError();

/**
 * Set maximum Talos_Loop() and Talos_onError() frequency.
 * @param frequency Loop frequency in Hertz.
*/
void Core_setMaximumLoopFrequency(uint32_t frequency);

/**
 * Set minimum Talos_Loop() and Talos_onError() period.
 * @param period In microseconds.
*/
void Core_setMinimumLoopPeriod(uint32_t period);

/**
 * This function will loop whatever Talos state.
 */
void __attribute__((weak)) Core_priorityLoop();

#endif